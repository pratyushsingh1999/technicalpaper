#Report on FLEX-BOX 

##What is a flex-box

- Flex-box is a CSS property that is applied to div containers or any element which possesses one or more child elements.
- The main benefit of using a flex-box is that it can make our website mobile-friendly without using media-queries.
- It helps us to place the content accurately over the frontend of the website.
- There are mainly 2 axes in a flex-box, i.e
    - row / row-reverse
    - column / column-reverse
    - These are defined by an additional property known as flex-direction
    - For eg:-      
    ```
           .container{
             display: flex;
             Flex-direction: column; ( /*By default its row*/)
             }
    ```
- Row-reverse and column-reverse will just place the child-content in reverse of the original order.
##When and where is a flex-box used?

- Flex-box is used to make the content and position of your HTML page change with the respective change in screen-size. 
- It is used to make your website more reactive and mobile-friendly (small-screen devices) for users opening websites on phones.
- It is generally used as a wrapper so that all the child elements are able to place themselves respective to the parent on which it was applied.
- Flex-box doesn’t affect the child of child elements. They will only affect their child elements.

##Some properties of Flex-Box
####Flex-wrap
- It’s a very important part of the flex-box module because it tells that weather the flex-box container should wrap the elements or not based on the screen size available.
- The values which can be assigned to a flex-wrap are:-
    - wrap
    - nowrap
    - Wrap-reverse
    - For eg:- If we have a div like this
    
      ```
      <div class=”flex-wrapper”>
        <p>Some content 1</p>
        <p>Some content 2</p>
        <p>Some content 3</p>
      </div>
      ```
      
      Now we will edit the class flex-wrapper
        ```      
        .flex-wrapper {
           display: flex;
           flex-wrap: wrap;
         }
        ```
              
      What will happen is that it will wrap the elements if the screen width is too small to accumulate all the elements.
         ``` 
           .flex-wrapper {
             display: flex;
             flex-wrap: nowrap;
           }
         ```
      Now what will happen is it won’t wrap the elements even if the screen width is too small this will result in deformation of your current element or overlapping of content on the frontend.
         ```
          .flex-wrapper {
             display: flex;
             flex-wrap: wrap-reverse;
          }
         ```                         
- It works exactly like a wrap but just the direction of the wrap is in the reverse direction.
####Flex-flow
- It’s a shorthand for flex-direction and flex-wrap property
- For eg:- 
```
   .flex-wrapper {
     display: flex;
     flex-direction: column;
     flex-wrap: wrap;
   }
   .flex-wrapper {
     display: flex;
     flex-flow: column wrap;
   }
```
- Both of the above codes will result in the same.


####Justify-content
- It is a property which is used to align the items inside the flex-box container.
- The values which can be assigned to justify-content are:-
    - center (Align all the items evenly with respect to the center)
    - flex-start (Align items to the start of the flex-box its also the default value)
    - flex-end (Align items to the end of the flex-box)
    - space-around (Space before, between, and after the items remain the same)
    - space-between (Space between all the elements is equal, space before and after is 0)
    - Space-evenly (Equal space everywhere)
- It’s one of the best features of flex-box because it helps in the dynamic placement of the items inside the flexbox based on your screen-size.

####Align-self
- It is a property that is used to align child items inside a flex-container to align themselves with respect to their size in the container.
- The values which can be assigned to align-self are:-
  - flex-start | flex-end | center | baseline | stretch
- Other types of CSS properties like float, clear, and vertical-align won’t work.


#####References
- https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox
- https://css-tricks.com/snippets/css/a-guide-to-flexbox/
- https://www.w3schools.com/css/css3_flexbox.asp
 
###Happy Coding !!
